package thread.extend;

public class MyTask extends Thread {

	private Integer number;
	private Integer printDelay;

	public MyTask() {
		this.number = 0;
	}

	public MyTask(Integer number, Integer printDelay) {
		this.number = number;
		this.printDelay = printDelay * 1000;
	}

	@Override
	public void run() {
		doWork();
	}

	public void doWork() {
		int counter = 1;
		while (true) {
			System.out.println(this.number * counter++);
			try {
				Thread.sleep(this.printDelay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
