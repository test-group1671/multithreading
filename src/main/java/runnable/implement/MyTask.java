package runnable.implement;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

public class MyTask implements Runnable {

	private Double number;
	private Integer waittime;

	public MyTask() {
		this.number = 0.0;
		this.waittime = 2;
	}

	public MyTask(Double number, Integer waittime) {
		this.number = number;
		this.waittime = waittime;
	}

	@Override
	public void run() {
		System.out.println("Started Execution of " + Thread.currentThread().getName());
		Double result = this.doCPUIntenseTask();
	}

	public Double doCPUIntenseTask() {
		Double result = 0.0;
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.SECOND, this.waittime);

		double counter = 0.0000001;
		while (Calendar.getInstance().compareTo(calender) < 0) {
			result = result + counter;
			this.writeDataInFile(result);
		}
		return result;
	}

	public void writeDataInFile(Double data) {
		File file = new File(this.number.toString());
		try {
			PrintWriter output = new PrintWriter(file);
			output.println(Thread.currentThread().getName() + " : " + data.toString());
			output.close();
		} catch (IOException ex) {
			System.out.println("Error:" + ex);
		}
		System.out.println(Thread.currentThread().getName() + " : " + data.toString());
	}

}
