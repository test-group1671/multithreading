package runnable.implement;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException, InterruptedException {

		for (int i = 1; i <= 12000; i++) {
			new Thread(new MyTask((double) i, 10)).start();
		}

	}

}
